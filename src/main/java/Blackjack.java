 

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Blackjack {
	
//	Singleton
	private static Blackjack instance;
	
	private Deck deck;
	private List<Card> hand;
	private List<Card> dealerHand;
	private final int limit = 21;
	private Scanner sc = new Scanner(System.in);
	private int playerWins, dealerWins, ties;
	
	
//	Privat konstruktor
	private Blackjack() {
		
		deck = new Deck();

		
	}
	
//	Metod för att hämta eller skapa en instans av Blackjack (Singleton)
	public static Blackjack getInstance() {
		if (instance == null) {
			instance = new Blackjack();
		}
		return instance;
	}
	

	public void game() {
//		Huvudmetoden för spelet
		
		String continueGame = "y";
		
//		Loopa så länge spelomgången pågår. Kommer köras minst en gång.
		do {
			
			reset();
			
//			Loopa så länge spelaren önskar ytterligare ett kort.
			while (true) {
				System.out.println("Do you want another card? (y/n)");
				String choice = sc.nextLine();
				if (!choice.toLowerCase().equals("y")) {
					break;
				}
				hit();
				if (score(hand) > limit) {
					System.out.println("You broke.....hahahahahahaha!!!!!!");
					break;
				} else if (score(hand) == limit) {
					System.out.println("Congratulations...You've got BlackJack!!!");
					break;
				}
				
			}
			
			if (score(hand)<= limit) {
				stand();
				
				if (score(dealerHand) > limit) {
					System.out.print("You won!\n");
					playerWins++;
				} else if (score(dealerHand) > score(hand) && score(dealerHand) <= limit) {
					System.out.print("You lost!\n");
					dealerWins++;
				} else if (score(dealerHand) < score(hand)) {
					System.out.print("You won!\n");
					playerWins++;
				} else if (score(dealerHand) == score(hand)) {
					System.out.print("It's a draw! (Which means there are two losers)\n");
					ties++;
				}	
			} else {
				dealerWins++;
			}
			System.out.printf("The current score is:\nPlayerWins: %d\nDealerWins: %d\nTies: %d\n\n", playerWins, dealerWins, ties);
			
			
			System.out.println("Do you want to play another round? (y/n)");
			continueGame = sc.nextLine().toLowerCase();
			
			
		} while (continueGame.equals("y"));
		
//		Spelet har avslutats. Stänger resurser.
		sc.close();
		
	}
	

	public void hit() {
//		Metod där spelaren får ett till kort
		
		Card tempCard = deck.draw();
		hand.add(tempCard);
		System.out.printf("You got %s", tempCard.toString().toLowerCase());
		System.out.printf("(%d points). \n", score(hand));
	
	}
	
	public void stand() {
//		Metod där dealern får kort tills summan av korten är över eller lika med 17. Presentera resultat.
		
		while (score(dealerHand) <= 17) {
			dealerHand.add(deck.draw());
		}
		presentResults();
		
	}
	
	public void reset() {
		
//		Sätter upp ett nytt spel med ett nytt deck och nya listor. 
		
		deck = new Deck();
		hand = new ArrayList<>();
		dealerHand = new ArrayList<>();
		
//		Dealern får ett kort
		dealerHand.add(deck.draw());
		
//		Spelaren får två kort;
		hit();
		hit();
		
//		Aktuell ställning
		presentResults();
	
	}
	
	public int score(List<Card> handToCheck) {
//		Made public for testing
//		Metod för att räkna ut poängen från summan av korten på hand. Tar aktuell hand som argument (spelare eller dealer).
		
		int points = 0;
			
		for (Card card : handToCheck) {
			points += getValue(card);
		}
		
//		Om summan är över 21 (limit) och minst ett ess finns på hand, räkna ess med värdet 1.
		if (points > limit) {
			for (Card card : handToCheck) {
				if (card.getRank().equals("Ace")) {
					points -= 10;
					if (points < limit) {
						return points;
					}
				}
			}
			
		}
		
		return points;
	}
	
	public int getValue (Card card) {

		
//		Metoden kollar värdet på kortet genom att kontrollera valören. 
		if (card.getRank().equals("King") || card.getRank().equals("Queen") || card.getRank().equals("Jack")) {
			return 10;
		} else if (card.getRank().equals("Ace")) {
			return 11;
		} else {
			for (int i = 0 ; i < deck.getRanks().length - 4; i++) {
				if (card.getRank().equals(deck.getRanks()[i])) {
					return i+2;
				}
			}
		}

		return -1;
		
	}
	
	public void setListHand (List<Card> newHand) {
		
		hand = newHand;
	}
	
	public List<Card> getPlayerHand () {
		return hand;
	}
	
	private void presentResults () {
//		Skriv ut vilka kort spelare och dealer har, samt poängställning i aktuell runda.
		
		System.out.println("\n\nPlayer has");
		for (Card card : hand) {
			System.out.println(card);
		}
		System.out.printf("Total points: %d\n\n", score(hand) );
		
		System.out.println("Dealer has");
		for (Card card : dealerHand) {
			System.out.println(card);
		}
		System.out.printf("Total points: %d\n\n", score(dealerHand));

	}
	
	public List<Card> getDealerHand () {
		return dealerHand;
	}
	
	

}
