


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


public class BlackjackTest {
	
	Blackjack blackjack;
	
	
	@Test
	void testScore () {
		
//		Method checks that score is correctly calculated
		assertEquals(18, checkScore(new Card(Suit.DIAMONDS, "Ace"), new Card(Suit.HEARTS, "Four"), new Card(Suit.SPADES, "Three")));
	}
	
	@Test
	void testScoreLowAce () {
//		Method makes sure that the value of ace is reduced when bust
		assertEquals(15, checkScore(new Card(Suit.CLUBS, "Jack"), new Card(Suit.HEARTS, "Four"), new Card(Suit.SPADES, "Ace")));
	}
	
	@Test
	void testScoreHighAndLowAce () {
//		Check that only the required no of aces are reduced with ten.  
		assertEquals(19, checkScore(new Card(Suit.CLUBS, "Seven"), new Card(Suit.HEARTS, "Ace"), new Card(Suit.SPADES, "Ace")));
	}
	
	
	
	int checkScore(Card c1, Card c2, Card c3) {
//		Supportmethod that returns an int (the points) that creates an ArrayList and sends it to the score method. 

		List<Card> tempHand = new ArrayList<>();
		tempHand.add(c1);
		tempHand.add(c2);
		tempHand.add(c3);

		
		return blackjack.score(tempHand);
		
	}
	
	@BeforeEach
//	Before all testmethods - instantiate new BlackJack. 
	void instantiateBlackjack () {
		blackjack = Blackjack.getInstance();
	}
	
	@Test
	void testGetValue () {
//		Method to check the get value method
		Card card = new Card(Suit.CLUBS, "Seven");
		assertEquals(7, blackjack.getValue(card));
	}
	
	@ParameterizedTest
	@ValueSource(ints = {1, 2, 5})
//	method to check that hit() actually increases the number of cards on hand
	void testHitMethod (int noCards) {
		blackjack.reset(); //Instantiates the hand (and starts the hand with two cards)
		
		for (int i = 0 ; i < noCards ; i++ ) {
			blackjack.hit();
		}
		assertEquals(noCards + 2, blackjack.getPlayerHand().size());
	}
	
//	Test stand method to make sure the dealer gets cards until or passed 17 points. 
	@Test
	void testStandMethod () {
		blackjack.reset();
		blackjack.stand();
		assertTrue(17 <= blackjack.score(blackjack.getDealerHand()));
		
	}
	
	

	

	


}
