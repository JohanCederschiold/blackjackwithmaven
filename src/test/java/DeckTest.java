


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

public class DeckTest {
	
	Deck deck;
	
	@BeforeEach
	void instantiateDeck() {
//		Before each testmethod - instantiate new deck 
		deck = new Deck();
	}
	

	
	@Test
	void testNumberOfCardsShuffle () {
//		Checks that the number of cards are not affected by shuffle
		int initialSize = deck.getDeck().size();
		deck.shuffle();
		assertEquals(initialSize, deck.getDeck().size());
	}
	
	@Test
	void testNoDoubles () {
//		Makes sure no doubles are present
		assertTrue(doubleChecker());

	}
	
	@Test
	void testNoDoublesAfterShuffle () {
//		Makes sure no doubles are present after shuffling. 
		deck.shuffle();
		assertTrue(doubleChecker());
		
	}
	
	@RepeatedTest(10)
	void checkShuffle () {
//		Makes sure the shuffle actually shuffles the cards(10 repetitions to make sure)
		assertTrue(shuffleAndCheck());
	}
	
	
	@Test
	void checkCorrectNoCards () {
//		Makes sure that there are actually 52 cards and that the 53rd card is null. 
		
		for (int i = 0 ; i < 52 ; i++ ) {
			
			assertNotNull(deck.draw());
			
		}
		assertNull(deck.draw());
	}
	
	
	
	
	boolean doubleChecker () {
//		Support method that verifies noDoubles (true).

		for (int i = 0 ; i < deck.getDeck().size(); i++ ) {
			for (int j = 0 ; j < deck.getDeck().size() ; j++) {
				if (i != j) {
					if (deck.getDeck().get(i).equals(deck.getDeck().get(j))) {
						return false;
					}
				}
			}
		}
		return true;
	}
	

	
	
	boolean shuffleAndCheck () {
		
//		Supportmethod that makes sure the cards are actually shuffled. 
		
		List<Card> beforeShuffle = new ArrayList<>(deck.getDeck());
		deck.shuffle();
		List<Card> afterShuffle = new ArrayList<>(deck.getDeck());
		

		int minimumDifference = 45; //Minst 50 kort skall ha bytt plats
		
		for (int i = 0 ; i < afterShuffle.size() ; i++ ) {
			if (!beforeShuffle.get(i).equals(afterShuffle.get(i))) {
				minimumDifference--;
			}
			if (minimumDifference == 0) {
				return true;
			}

		
		}
		return false;
	}
	
	
	
	
}
